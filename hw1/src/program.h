﻿#pragma once
#include "common.h"
#include "shader.h"
#include <cmath>

#include "obj_loader.h"

class program_t
{

    static char const * MODEL_FILE_NAME;
    static char const * VS_FILE_NAME;
    static char const * FS_FILE_NAME;


public:
    program_t();
    ~program_t();

    void init_buffer();
    void init_vertex_array();
    void draw_frame( float time_from_start );

private:
    bool wireframe_;
    bool color_mode_;
    GLuint vs_, fs_, program_;
    GLuint vx_buf_, normal_buf_;
    GLuint vao_;
    quat   rotation_by_control_;

    vector<vec3> vertices;
    vector<vec2> textures;
    vector<vec3> normals;

    float v_, k_, max_;
    vec3 center_;

    void calculate_color_params(mat4 model_view);
};

