#version 130

in vec3 in_pos;
in vec3 in_normal;

out vec4 normal;
out vec3 out_pos;

uniform mat4 mvp;
uniform mat4 model_view;

void main()
{
    gl_Position = mvp * vec4(in_pos, 1);	
    normal = model_view * vec4(in_normal, 1);
    out_pos = in_pos;
}
