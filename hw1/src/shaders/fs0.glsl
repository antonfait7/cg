#version 130

#define M_PI 3.1415926535897932384626433832795

in vec4 normal;
in vec3 out_pos;

out vec3 o_color;

uniform bool wireframe;
uniform bool is_normal_color_mode;

uniform float time;
uniform float k;
uniform float v;
uniform vec3 center;
uniform float max;

void main()
{

	
    if(wireframe) {
        o_color = vec3(1.0f, 1.0f, 1.0f);
    } else {
        if(is_normal_color_mode) {
            o_color = vec3(normal.y);
        } else {
            o_color = vec3(sin(2 * M_PI * (v * time + length(out_pos - center) * k * center / max)));
        }
    }    
}
