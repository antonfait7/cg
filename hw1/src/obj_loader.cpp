﻿#include "obj_loader.h"

string const obj_loader::VERTEX_HEADER = "v";
string const obj_loader::TEXTURE_HEADER = "vt";
string const obj_loader::NORMAL_HEADER = "vn";
string const obj_loader::FACE_HEADER = "f";


bool obj_loader::load(const char * file_name, vector<vec3> & out_vertices, vector<vec2> & out_textures, vector<vec3> & out_normals){

    std::vector<uint> vertex_indices, texture_indices, normal_indices;
    std::vector<vec3> temp_vertices;
    std::vector<vec2> temp_textures;
    std::vector<vec3> temp_normals;

    ifstream file(file_name);

    if (!file.is_open()) {
        throw std::runtime_error(string(file_name) + " file is not opened");
    }


    while(!file.eof()) {
        string line;
        getline(file, line);
        if (line.length() == 0) {
            continue;
        }
        if (strncmp(line.c_str(), TEXTURE_HEADER.c_str(), TEXTURE_HEADER.length()) == 0) {
            vec2 texture;
            string template_str = TEXTURE_HEADER + " %f %f";
            sscanf(line.c_str(), template_str.c_str(), &texture.x, &texture.y);
            temp_textures.push_back(texture);
        }
        else if (strncmp(line.c_str(), NORMAL_HEADER.c_str(), NORMAL_HEADER.length()) == 0) {
            vec3 normal;
            string template_str = NORMAL_HEADER + " %f %f %f";
            sscanf(line.c_str(), template_str.c_str(), &normal.x, &normal.y, &normal.z);
            temp_normals.push_back(normal);
        }
        else if (strncmp(line.c_str(), VERTEX_HEADER.c_str(), VERTEX_HEADER.length()) == 0) {
            vec3 vertex;
            string template_str = VERTEX_HEADER + " %f %f %f";
            sscanf(line.c_str(), template_str.c_str(), &vertex.x, &vertex.y, &vertex.z);
            temp_vertices.push_back(vertex);
        }
        else if (strncmp(line.c_str(), FACE_HEADER.c_str(), FACE_HEADER.length()) == 0) {
            uint vertex_index[3], texture_index[3], normal_index[3];
            string template_str = FACE_HEADER + " %d/%d/%d %d/%d/%d %d/%d/%d";
            int matches = sscanf(
                        line.c_str(), template_str.c_str(),
                        &vertex_index[0], &texture_index[0], &normal_index[0],
                        &vertex_index[1], &texture_index[1], &normal_index[1],
                        &vertex_index[2], &texture_index[2], &normal_index[2]
                    );
            if (matches != 9){
                throw std::runtime_error("Inexpected format of obj file: " + line);
            }
            vertex_indices.push_back(vertex_index[0]);
            vertex_indices.push_back(vertex_index[1]);
            vertex_indices.push_back(vertex_index[2]);
            texture_indices.push_back(texture_index[0]);
            texture_indices.push_back(texture_index[1]);
            texture_indices.push_back(texture_index[2]);
            normal_indices.push_back(normal_index[0]);
            normal_indices.push_back(normal_index[1]);
            normal_indices.push_back(normal_index[2]);
        }
    }

    for (size_t i = 0; i < vertex_indices.size(); ++i) {
        out_vertices.push_back(temp_vertices[--vertex_indices[i]]);
        out_textures.push_back(temp_textures[--texture_indices[i]]);
        out_normals.push_back(temp_normals[--normal_indices[i]]);
    }

}
