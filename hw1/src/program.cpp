﻿#include "program.h"

char const * program_t::MODEL_FILE_NAME = "..//src//model.obj";
char const * program_t::VS_FILE_NAME = "..//src//shaders//vs0.glsl";
char const * program_t::FS_FILE_NAME = "..//src//shaders//fs0.glsl";


void TW_CALL toggle_fullscreen_callback( void * )
{
    glutFullScreenToggle();
}

program_t::program_t()
   : wireframe_(false),
     color_mode_(true),
     v_(1.0f),
     k_(1.0f),
     center_(vec3(0.0f,0.0f,0.0f)),
     max_(0.0f)
{
#ifdef USE_CORE_OPENGL
    TwInit(TW_OPENGL_CORE, NULL);
#else
    TwInit(TW_OPENGL, NULL);
#endif
    // Определение "контролов" GUI
    TwBar *bar = TwNewBar("Parameters");
    TwDefine(" Parameters size='500 150' color='70 100 120' valueswidth=220 iconpos=topleft");
    TwAddVarRW(bar, "v", TW_TYPE_FLOAT, &v_, " min=-1000 max=1000 step=1 label='V' keyincr=q keydecr=a");
    TwAddVarRW(bar, "k", TW_TYPE_FLOAT, &k_, " min=-1000 max=1000 step=1 label='K' keyincr=w keydecr=s");
    TwAddVarRW(bar, "Wireframe mode", TW_TYPE_BOOLCPP, &wireframe_, " true='ON' false='OFF' key=e");
    TwAddVarRW(bar, "Color mode", TW_TYPE_BOOLCPP, &color_mode_, " true='Normal' false='Functional' key=r");
    TwAddButton(bar, "Fullscreen toggle", toggle_fullscreen_callback, NULL,
               " label='Toggle fullscreen mode' key=f");
    TwAddVarRW(bar, "ObjRotation", TW_TYPE_QUAT4F, &rotation_by_control_,
              " label='Object orientation' opened=true help='Change the object orientation.' ");


    vs_ = create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME);
    fs_ = create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME);
    program_ = create_program(vs_, fs_);

    obj_loader::load(MODEL_FILE_NAME, vertices, textures, normals);

    init_buffer();
    init_vertex_array();

}

program_t::~program_t()
{
    glDeleteProgram(program_);
    glDeleteShader(vs_);
    glDeleteShader(fs_);
    glDeleteVertexArrays(1, &vao_);
    glDeleteBuffers(1, &vx_buf_);
    glDeleteBuffers(1, &normal_buf_);

    TwDeleteAllBars();
    TwTerminate();
}

void program_t::init_buffer()
{
    glGenBuffers(1, &vx_buf_);
    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec3), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &normal_buf_);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buf_);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void program_t::init_vertex_array()
{
    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);

    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_);
    // запрашиваем индек аттрибута у программы, созданные по входным шейдерам
    GLuint const pos_location = glGetAttribLocation(program_, "in_pos");
    glVertexAttribPointer(pos_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    // "включаем" аттрибут "pos_location"
    glEnableVertexAttribArray(pos_location);

    glBindBuffer(GL_ARRAY_BUFFER, normal_buf_);
    GLuint normal_location = glGetAttribLocation(program_, "in_normal");
    glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(normal_location);

    glBindVertexArray(0);
}

void program_t::calculate_color_params(mat4 model_view) {
    center_ = center_ / float(vertices.size());
    for (size_t i = 0; i < vertices.size(); ++i) {
        center_ += vertices[i];
    }
    for (size_t i = 0; i < vertices.size(); ++i) {
        if (length(center_ - vertices[i]) > max_) {
            max_ = length(center_ - vertices[i]);
        }
    }
    center_ = vec3(model_view * vec4(center_, 1));
}

void program_t::draw_frame( float time_from_start )
{

    float const w = (float)glutGet(GLUT_WINDOW_WIDTH);
    float const h = (float)glutGet(GLUT_WINDOW_HEIGHT);


    mat4  const proj = perspective(45.0f, w / h, 0.1f, 100.0f);
    mat4  const view = lookAt(vec3(0, 0, 20), vec3(0, 0, 0), vec3(0, 1, 0));
    mat4 const model = mat4_cast(rotation_by_control_);

    mat4 const model_view = view * model;
    mat4 const mvp = proj * model_view;

    calculate_color_params(model_view);

    glClearColor(0.7f, 0.7f, 0.7f, 1);
    glClearDepth(1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // установка шейдеров для рисования
    glUseProgram(program_);


    GLuint const model_view_location = glGetUniformLocation(program_, "model_view");
    glUniformMatrix4fv(model_view_location, 1, GL_FALSE, &model_view[0][0]);

    GLuint const mvp_location = glGetUniformLocation(program_, "mvp");
    glUniformMatrix4fv(mvp_location, 1, GL_FALSE, &mvp[0][0]);


    GLuint const wireframe_location = glGetUniformLocation(program_, "wireframe");
    glUniform1ui(wireframe_location, false);

    GLuint const normal_color_mode_location = glGetUniformLocation(program_, "is_normal_color_mode");
    glUniform1i(normal_color_mode_location, color_mode_);

    GLuint const time_location = glGetUniformLocation(program_, "time");
    glUniform1f(time_location, time_from_start);

    GLuint const k_location = glGetUniformLocation(program_, "k");
    glUniform1f(k_location, k_);

    GLuint const v_location = glGetUniformLocation(program_, "v");
    glUniform1f(v_location, v_);

    GLuint const center_location = glGetUniformLocation(program_, "center");
    glUniform3f(center_location, center_[0], center_[1], center_[2]);

    GLuint const max_location = glGetUniformLocation(program_, "max");
    glUniform1f(max_location, max_);

    glBindVertexArray(vao_);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawArrays(GL_TRIANGLES, 0, 3 * vertices.size());

    if (wireframe_) {


        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(-1, -1);


        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        GLuint const wireframe_location = glGetUniformLocation(program_, "wireframe");
        glUniform1ui(wireframe_location, true);

        glDrawArrays(GL_TRIANGLES, 0, 3 * vertices.size());
        glDisable(GL_POLYGON_OFFSET_FILL);
    }



    glBindVertexArray(0);

}

