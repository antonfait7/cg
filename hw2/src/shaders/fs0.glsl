#version 130

in vec4 normal;
in vec2 texture_uv;

out vec3 o_color;

uniform bool wireframe;
uniform sampler2D in_texture;
uniform sampler2D in_normal_texture;

void main()
{
    vec3 texture_color = texture(in_texture, texture_uv).rgb;
    
    if(wireframe) {
        o_color = vec3(1.0f, 1.0f, 1.0f);
    } else {
        o_color = texture_color;
    }    
}
