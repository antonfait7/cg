#version 130

in vec3 in_pos;
in vec3 in_normal;
in vec2 in_texture_uv;

out vec4 normal;
out vec2 texture_uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float texture_coords_scale;

void main()
{
    gl_Position = projection * view * model * vec4(in_pos, 1);
    texture_uv = texture_coords_scale * in_texture_uv;	
    normal = view * model * vec4(in_normal, 1);
}
