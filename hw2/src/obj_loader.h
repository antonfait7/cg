﻿#pragma once


#include <cstdio>
using std::sscanf;

#include "common.h"

#include <stdio.h>

class obj_loader {
public:

    static string const VERTEX_HEADER;
    static string const TEXTURE_HEADER;
    static string const NORMAL_HEADER;
    static string const FACE_HEADER;

    static bool load(
            char const * file_name,
            vector<vec3> & out_vertices,
            vector<vec2> & out_textures,
            vector<vec3> & out_normals
    );

    static GLuint loadTexture(const char * image_file_name);
};
