﻿#include "common.h"
#include "shader.h"
#include <cmath>


#include "program.h"



#ifndef APIENTRY
   #define APIENTRY
#endif

unique_ptr<program_t> g_program;

void display_func()
{
    static chrono::system_clock::time_point const start = chrono::system_clock::now();

    g_program->draw_frame(chrono::duration<float>(chrono::system_clock::now() - start).count());

    TwDraw();

    glutSwapBuffers();
}

void idle_func()
{
    glutPostRedisplay();
}

void keyboard_func( unsigned char button, int x, int y )
{
    if (TwEventKeyboardGLUT(button, x, y))
        return;

    switch(button)
    {
    case 27:
        exit(0);
    }
}

void reshape_func( int width, int height )
{
    if (width <= 0 || height <= 0)
        return;
    glViewport(0, 0, width, height);
    TwWindowSize(width, height);
}

void close_func()
{
    g_program.reset();
}

// callback на различные сообщения от OpenGL
void APIENTRY gl_debug_proc(  GLenum         //source
                              , GLenum         type
                              , GLuint         //id
                              , GLenum         //severity
                              , GLsizei        //length
                              , GLchar const * message
                              
                              , GLvoid * //user_param
                                )
{
    if (type == GL_DEBUG_TYPE_ERROR_ARB)
    {
        cerr << message << endl;
        exit(1);
    }
}

int main( int argc, char ** argv )
{
    size_t const default_width  = 800;
    size_t const default_height = 800;



    glutInit               (&argc, argv);
    glutInitWindowSize     (default_width, default_height);
    glutInitDisplayMode    (GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitContextVersion (3, 0);

    int window_handle = glutCreateWindow("OpenGL hw1");

    if (glewInit() != GLEW_OK)
    {
        cerr << "GLEW init failed" << endl;
        return 1;
    }
    if (!GLEW_VERSION_3_0)
    {
        cerr << "OpenGL 3.0 not supported" << endl;
        return 1;
    }


#ifdef USE_CORE_OPENGL
    glutDestroyWindow(window_handle);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    window_handle = glutCreateWindow("OpenGL basic sample");
#endif

    // Трассировка ошибок по callback'у
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    glDebugMessageCallbackARB(gl_debug_proc, NULL);
    // выключить все трассировки
    glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE           , GL_DONT_CARE, 0, NULL, false);
    // включить сообщения только об ошибках
    glDebugMessageControlARB(GL_DONT_CARE, GL_DEBUG_TYPE_ERROR_ARB, GL_DONT_CARE, 0, NULL, true );

    // подписываемся на оконные события
    glutReshapeFunc(reshape_func);
    glutDisplayFunc(display_func);
    glutIdleFunc   (idle_func   );
    glutCloseFunc  (close_func  );
    glutKeyboardFunc(keyboard_func);

    // подписываемся на события для AntTweakBar'а
    glutMouseFunc        ((GLUTmousebuttonfun)TwEventMouseButtonGLUT);
    glutMotionFunc       ((GLUTmousemotionfun)TwEventMouseMotionGLUT);
    glutPassiveMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
    glutSpecialFunc      ((GLUTspecialfun    )TwEventSpecialGLUT    );
    TwGLUTModifiersFunc  (glutGetModifiers);

    try
    {
        g_program.reset(new program_t());
        glutMainLoop();
    }
    catch( std::exception const & except )
    {
        std::cout << except.what() << endl;
        return 1;
    }

    return 0;
}
