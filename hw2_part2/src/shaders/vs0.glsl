#version 130

in vec3 in_pos;
in vec3 in_normal;
in vec2 in_texture_uv;
in vec3 in_tangent;
in vec3 in_bitangent;

out vec2 texture_uv;
out vec3 eye_dir_tangent_space;
out vec3 light_dir_tangent_space;

uniform mat4 mvp;
uniform mat4 model_view;
uniform mat3 model_view3;

uniform float texture_coords_scale;
uniform vec3 light_dir;

void main()
{
    gl_Position = mvp * vec4(in_pos, 1);
    texture_uv = texture_coords_scale * in_texture_uv;
    vec3 normal_camera_space = model_view3 * normalize(in_normal);
    vec3 tangent_camera_space = model_view3 * normalize(in_tangent);
    vec3 bitangent_camera_space = model_view3 * normalize(in_bitangent);
    mat3 TBN = transpose(mat3(tangent_camera_space, bitangent_camera_space, normal_camera_space));

    light_dir_tangent_space = TBN * (-light_dir);
    eye_dir_tangent_space = TBN * (-(model_view * vec4(in_pos, 1)).xyz);    
}
