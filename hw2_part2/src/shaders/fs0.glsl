#version 130

in vec2 texture_uv;
in vec3 eye_dir_tangent_space;
in vec3 light_dir_tangent_space;

out vec3 o_color;

uniform bool wireframe;
uniform sampler2D in_texture;
uniform sampler2D in_normal_texture;


uniform vec3 light_color;
uniform vec3 specular;
uniform vec3 ambient;

uniform float light_power;
uniform float specular_power;

void main()
{
    if(wireframe) {
        o_color = vec3(1.0f, 1.0f, 1.0f);
    } else {
        vec3 texture_color = texture(in_texture, texture_uv).rgb;
        vec3 ambient_part = ambient * texture_color;
        vec3 n = normalize(texture(in_normal_texture, texture_uv).rgb * 2.0 - 1.0);
        vec3 l = normalize(light_dir_tangent_space);
        vec3 diffuse_part = clamp(dot(n, l), 0, 1) * texture_color * light_color * light_power;

        vec3 E = normalize(eye_dir_tangent_space);
        vec3 R = reflect(-l, n);
        float cos_a = clamp(dot(E, R), 0, 1);
        vec3 specular_part = specular * pow(cos_a, specular_power);
        o_color = ambient_part + diffuse_part + specular_part;        
    }    
}
