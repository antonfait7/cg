﻿#pragma once
#include "common.h"
#include "shader.h"
#include <cmath>

#include "obj_loader.h"

class program_t
{
    static char const* TEXTURE_NAME;
    static char const* NORMAL_TEXTURE_NAME;

    static char const * MODEL_FILE_NAME[4];

    static size_t const MODEL_NUM;
    static size_t const FILTRATION_MODE_NUM;

    static char const * VS_FILE_NAME;
    static char const * FS_FILE_NAME;

    enum filtering_mode { NEAREST, LINEAR, MIPMAP };

    GLuint vs_, fs_, program_;
    GLuint vx_buf_[4], normal_buf_[4], texture_buf_[4], tangent_buf_[4], bitangent_buf_[4];
    GLuint vao_[4];
    GLuint texture_, normal_texture_;
    GLuint texture_location_, normal_texture_location_;
    quat   rotation_by_control_;

    bool wireframe_;
    size_t current_model_num_;
    size_t current_filtration_mode_;
    float texture_coords_scale_;

    vector<vec3> vertices[4];
    vector<vec2> textures[4];
    vector<vec3> normals[4];
    vector<vec3> tangents[4];
    vector<vec3> bitangents[4];

    float light_power_;
    double light_dir_[3];
    float light_color_[3];
    float ambient_[3];
    float specular_[3];
    float specular_power_;

public:
    program_t();
    ~program_t();

    void init_buffer(size_t num);
    void init_vertex_array(size_t num);
    void init_attributes(size_t num);
    void init_texture();

    void draw_frame( float time_from_start );

    static void model_change_callback(void* program_wrapper);
    static void filtration_change_callback(void* program_wrapper);

private:
    void run_texture_filter();
    void calculate_tangent_basis(vector<vec3> & tangents, vector<vec3> & bitangents, size_t num);

};

