﻿#pragma once

#include <cstddef>
#include <vector>
using std::vector;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;
using std::getline;

#include <iostream>
using std::cerr;
using std::endl;
using std::cout;

#include <chrono>
namespace chrono = std::chrono;

#include <memory>
using std::unique_ptr;

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <AntTweakBar/include/AntTweakBar.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>


using namespace glm;
