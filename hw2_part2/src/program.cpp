﻿
#include "program.h"

char const* program_t::TEXTURE_NAME = "..//src//data//texture.bmp";
char const* program_t::NORMAL_TEXTURE_NAME = "..//src//data//normal.bmp";

char const * program_t::MODEL_FILE_NAME[4] = {"..//src//data//sphere.obj",
                                             "..//src//data//cylinder.obj",
                                             "..//src//data//quad.obj",
                                              "..//src//data//model.obj"
                                             };

size_t const program_t::MODEL_NUM = 4;

size_t const program_t::FILTRATION_MODE_NUM = 3;

char const * program_t::VS_FILE_NAME = "..//src//shaders//vs0.glsl";
char const * program_t::FS_FILE_NAME = "..//src//shaders//fs0.glsl";


void TW_CALL toggle_fullscreen_callback( void * )
{
    glutFullScreenToggle();
}

void program_t::model_change_callback(void* program_wrapper) {

    program_t* p = static_cast<program_t*>(program_wrapper);
    p->current_model_num_ = (p->current_model_num_ + 1) % MODEL_NUM;
}

void program_t::filtration_change_callback(void* program_wrapper) {

    program_t* p = static_cast<program_t*>(program_wrapper);
    p->current_filtration_mode_ = (p->current_filtration_mode_ + 1) % FILTRATION_MODE_NUM;
}

program_t::program_t()
   : wireframe_(false),
     current_model_num_(0),
     current_filtration_mode_(0),
     texture_coords_scale_(1.0),
     light_power_(1.0),
     specular_power_(5.0)

{
#ifdef USE_CORE_OPENGL
    TwInit(TW_OPENGL_CORE, NULL);
#else
    TwInit(TW_OPENGL, NULL);
#endif
    // Определение "контролов" GUI
    TwBar *bar = TwNewBar("Parameters");
    TwDefine(" Parameters size='300 500' color='70 100 120' valueswidth=220 iconpos=topleft");
    TwAddButton(bar, "Change model", model_change_callback, this,
               " label='Change model' key=q");
    TwAddVarRW(bar, "ObjRotation", TW_TYPE_QUAT4F, &rotation_by_control_,
              " label='Object orientation' opened=true help='Change the object orientation.' ");

    TwAddButton(bar, "Change filtration mode", filtration_change_callback, this,
               " label='Change filtration mode: MIPMAP LINEAR NEAREST'  key=w");
    TwAddVarRW(bar, "Wireframe mode", TW_TYPE_BOOLCPP, &wireframe_, " true='ON' false='OFF' key=e");

    TwAddVarRW(bar, "Texture coords scale", TW_TYPE_FLOAT, &texture_coords_scale_,
                   "min=0.1 max=10 step=0.1 keyincr=s keydecr=x");

    TwAddVarRW(bar, "Light Power ", TW_TYPE_FLOAT, &light_power_, "min=0 max=10 step=0.1");
    TwAddVarRW(bar, "Light Direction ", TW_TYPE_DIR3D, &light_dir_, "");
    TwAddVarRW(bar, "Light Color ", TW_TYPE_COLOR3F, &light_color_, " colormode=rgb ");
    TwAddVarRW(bar, "Ambient ", TW_TYPE_COLOR3F, &ambient_, " colormode=hls ");
    TwAddVarRW(bar, "Specular ", TW_TYPE_COLOR3F, &specular_, " colormode=rgb ");
    TwAddVarRW(bar, "Specular Power ", TW_TYPE_FLOAT, &specular_power_, "min=0 max=100 step=0.1");

    TwAddButton(bar, "Fullscreen toggle", toggle_fullscreen_callback, NULL,
               " label='Toggle fullscreen mode' key=f");


    light_dir_[0] = 1;
    light_dir_[1] = -5;
    light_dir_[2] = -10;
    light_color_[0] = 1.0f;
    light_color_[1] = 1.0f;
    light_color_[2] = 1.0f;
    ambient_[0] = 0.0f;
    ambient_[1] = 0.0f;
    ambient_[2] = 0.0f;
    specular_[0] = 1.0f;
    specular_[1] = 1.0f;
    specular_[2] = 1.0f;

    vs_ = create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME);
    fs_ = create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME);
    program_ = create_program(vs_, fs_);


    for (size_t i = 0; i < MODEL_NUM; ++i) {
        obj_loader::load(MODEL_FILE_NAME[i], vertices[i], textures[i], normals[i]);
        calculate_tangent_basis(tangents[i], bitangents[i], i);
        init_buffer(i);
        init_vertex_array(i);
        init_attributes(i);
    }

    init_texture();

}

program_t::~program_t()
{
    glDeleteProgram(program_);
    glDeleteShader(vs_);
    glDeleteShader(fs_);
    for (size_t i = 0; i < MODEL_NUM; ++i) {
        glDeleteVertexArrays(1, &vao_[i]);
        glDeleteBuffers(1, &vx_buf_[i]);
        glDeleteBuffers(1, &normal_buf_[i]);
        glDeleteBuffers(1, &texture_buf_[i]);
        glDeleteBuffers(1, &tangent_buf_[i]);
        glDeleteBuffers(1, &bitangent_buf_[i]);

    }

    TwDeleteAllBars();
    TwTerminate();
}

void program_t::init_buffer(size_t num)
{
    glGenBuffers(1, &vx_buf_[num]);
    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_[num]);
    glBufferData(GL_ARRAY_BUFFER, vertices[num].size() * sizeof(vec3), &vertices[num][0], GL_STATIC_DRAW);

    glGenBuffers(1, &texture_buf_[num]);
    glBindBuffer(GL_ARRAY_BUFFER, texture_buf_[num]);
    glBufferData(GL_ARRAY_BUFFER, textures[num].size() * sizeof(vec2), &textures[num][0], GL_STATIC_DRAW);

    glGenBuffers(1, &normal_buf_[num]);
    glBindBuffer(GL_ARRAY_BUFFER, normal_buf_[num]);
    glBufferData(GL_ARRAY_BUFFER, normals[num].size() * sizeof(vec3), &normals[num][0], GL_STATIC_DRAW);

    glGenBuffers(1, &tangent_buf_[num]);
    glBindBuffer(GL_ARRAY_BUFFER, tangent_buf_[num]);
    glBufferData(GL_ARRAY_BUFFER, tangents[num].size() * sizeof(vec3), &tangents[num][0], GL_STATIC_DRAW);

    glGenBuffers(1, &bitangent_buf_[num]);
    glBindBuffer(GL_ARRAY_BUFFER, bitangent_buf_[num]);
    glBufferData(GL_ARRAY_BUFFER, bitangents[num].size() * sizeof(vec3), &bitangents[num][0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void program_t::init_vertex_array(size_t num)
{
    glGenVertexArrays(1, &vao_[num]);

    glBindVertexArray(vao_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, normal_buf_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, texture_buf_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, tangent_buf_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, bitangent_buf_[num]);

    glBindVertexArray(0);
}


void program_t::init_attributes(size_t num) {
    glBindVertexArray(vao_[num]);

    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_[num]);
    // запрашиваем индек аттрибута у программы, созданные по входным шейдерам
    GLuint const pos_location = glGetAttribLocation(program_, "in_pos");
    glVertexAttribPointer(pos_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    // "включаем" аттрибут "pos_location"
    glEnableVertexAttribArray(pos_location);

    glBindBuffer(GL_ARRAY_BUFFER, normal_buf_[num]);
    GLuint normal_location = glGetAttribLocation(program_, "in_normal");
    glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(normal_location);

    glBindBuffer(GL_ARRAY_BUFFER, texture_buf_[num]);
    GLuint texture_uv_location = glGetAttribLocation(program_, "in_texture_uv");
    glVertexAttribPointer(texture_uv_location, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), 0);
    glEnableVertexAttribArray(texture_uv_location);

    glBindBuffer(GL_ARRAY_BUFFER, tangent_buf_[num]);
    GLuint tangents_location = glGetAttribLocation(program_, "in_tangent");
    glVertexAttribPointer(tangents_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(tangents_location);

    glBindBuffer(GL_ARRAY_BUFFER, bitangent_buf_[num]);
    GLuint bitangents_location = glGetAttribLocation(program_, "in_bitangent");
    glVertexAttribPointer(bitangents_location, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
    glEnableVertexAttribArray(bitangents_location);


    glBindVertexArray(0);
}


void program_t::init_texture() {
    texture_ = obj_loader::loadTexture(TEXTURE_NAME);
    normal_texture_ = obj_loader::loadTexture(NORMAL_TEXTURE_NAME);
    texture_location_ = glGetUniformLocation(program_, "in_texture");
    normal_texture_location_ = glGetUniformLocation(program_, "in_normal_texture");
}

void program_t::draw_frame( float time_from_start )
{

    float const w = (float)glutGet(GLUT_WINDOW_WIDTH);
    float const h = (float)glutGet(GLUT_WINDOW_HEIGHT);


    mat4 const proj = perspective(45.0f, w / h, 0.1f, 100.0f);
    mat4 const view = lookAt(vec3(0, 0, 7), vec3(0, 0, 0), vec3(0, 1, 0));
    mat4 const model = mat4_cast(rotation_by_control_);
    mat4 const model_view = view * model;
    mat4 const mvp = proj * model_view;
    mat3 const model_view3 = mat3(model_view);

    glClearColor(0.7f, 0.7f, 0.7f, 1);
    glClearDepth(1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // установка шейдеров для рисования
    glUseProgram(program_);

    GLuint const mvp_location = glGetUniformLocation(program_, "mvp");
    glUniformMatrix4fv(mvp_location, 1, GL_FALSE, &mvp[0][0]);

    GLuint const model_view_location = glGetUniformLocation(program_, "model_view");
    glUniformMatrix4fv(model_view_location, 1, GL_FALSE, &model_view[0][0]);

    GLuint const model_view3_location = glGetUniformLocation(program_, "model_view3");
    glUniformMatrix3fv(model_view3_location, 1, GL_FALSE, &model_view3[0][0]);

    GLuint const wireframe_location = glGetUniformLocation(program_, "wireframe");
    glUniform1ui(wireframe_location, false);

    glUniform1f(glGetUniformLocation(program_, "texture_coords_scale"), texture_coords_scale_);

    glUniform3f(glGetUniformLocation(program_, "light_dir"), light_dir_[0], light_dir_[1], light_dir_[2]);
    glUniform3f(glGetUniformLocation(program_, "light_color"), light_color_[0], light_color_[1], light_color_[2]);
    glUniform3f(glGetUniformLocation(program_, "ambient"), ambient_[0], ambient_[1], ambient_[2]);
    glUniform3f(glGetUniformLocation(program_, "specular"), specular_[0], specular_[1], specular_[2]);
    glUniform1f(glGetUniformLocation(program_, "light_power"), (GLfloat)light_power_);
    glUniform1f(glGetUniformLocation(program_, "specular_power"), (GLfloat)specular_power_);


    glBindVertexArray(vao_[current_model_num_]);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_);
    run_texture_filter();
    glUniform1i(texture_location_, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normal_texture_);
    run_texture_filter();
    glUniform1i(normal_texture_location_, 1);


    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawArrays(GL_TRIANGLES, 0, 3 * vertices[current_model_num_].size());

    if (wireframe_) {
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(-1, -1);


        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        GLuint const wireframe_location = glGetUniformLocation(program_, "wireframe");
        glUniform1ui(wireframe_location, true);

        glDrawArrays(GL_TRIANGLES, 0, 3 * vertices[current_model_num_].size());
        glDisable(GL_POLYGON_OFFSET_LINE);
    }

    glBindVertexArray(0);

}

void program_t::run_texture_filter() {
    switch (current_filtration_mode_) {
    case 0: //MIPMAL
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);
        break;
    case 1: //LINEAR
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        break;
    case 2: //NEAREST
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        break;
     default:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        break;
    }
}

void program_t::calculate_tangent_basis(vector<vec3> &tangents, vector<vec3> &bitangents, size_t num) {

    for (size_t i = 0; i < vertices[num].size(); i += 3) {

        vec3 & v0 = vertices[num][i + 0];
        vec3 & v1 = vertices[num][i + 1];
        vec3 & v2 = vertices[num][i + 2];
        vec2 & texture0 = textures[num][i + 0];
        vec2 & texture1 = textures[num][i + 1];
        vec2 & texture2 = textures[num][i + 2];

        vec3 delta_pos1 = v1 - v0;
        vec3 delta_pos2 = v2 - v0;
        vec2 delta_texture1 = texture1 - texture0;
        vec2 delta_texture2 = texture2 - texture0;

        float r = 1.0f / (delta_texture1.x * delta_texture2.y - delta_texture1.y * delta_texture2.x);
        vec3 tangent = (delta_pos1 * delta_texture2.y - delta_pos2 * delta_texture1.y) * r;
        vec3 bitangent = (delta_pos2 * delta_texture1.x - delta_pos1 * delta_texture2.x) * r;

        tangents.push_back(tangent);
        tangents.push_back(tangent);
        tangents.push_back(tangent);

        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);
    }

}

