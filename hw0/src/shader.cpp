﻿#include "shader.h"
#include <string>
#include <iostream>

GLuint create_shader( GLenum shader_type, char const * file_name )
{
    std::string shader_code;
    std::ifstream shader_stream(file_name, std::ios::in);
    if(shader_stream.is_open())
    {
        std::string line = "";
        while(getline(shader_stream, line))
            shader_code +=  line + "\n";
        shader_stream.close();
    } else {
        throw std::runtime_error(string("Shader source file ") + file_name + " access error");
    }


    GLchar const * gl_text = shader_code.c_str();
    std::cout<< gl_text << std::endl;
    size_t const size = shader_code.size();
    GLuint const shader = glCreateShader(shader_type);

    glShaderSource(shader, 1, &gl_text, (GLint*)&size);
    glCompileShader(shader);

    GLint result;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (!result)
    {
        int info_log_length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
        if (info_log_length > 0)
        {
            string Buffer;
            Buffer.resize(info_log_length);
            glGetShaderInfoLog(shader, info_log_length, NULL, &Buffer[0]);
            throw std::runtime_error(Buffer);
        }
    }

    return shader;
}

GLuint create_program( GLuint vs, GLuint fs )
{
    GLuint const program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    GLint result;
    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if(!result)
    {
        int info_log_length;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
        if (info_log_length > 0)
        {
            string Buffer;
            Buffer.resize(info_log_length);
            glGetProgramInfoLog(program, info_log_length, NULL, &Buffer[0]);
            throw std::runtime_error(Buffer);
        }
    }
    return program;
}
