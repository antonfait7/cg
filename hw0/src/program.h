﻿#pragma once
#include "common.h"
#include "shader.h"
#include <cmath>

class program_t
{
    static char const * VS_FILE_NAME0;
    static char const * FS_FILE_NAME0;

    static char const * VS_FILE_NAME1;
    static char const * FS_FILE_NAME1;

    static char const * VS_FILE_NAME2;
    static char const * FS_FILE_NAME2;

    static char const * VS_FILE_NAME3;
    static char const * FS_FILE_NAME3;

    static size_t const TASKS_NUM;

public:
    program_t();
    ~program_t();

    void init_buffer();
    void init_vertex_array();
    void init_in_pos(GLuint program);
    void draw_frame( float time_from_start );

    static void TW_CALL toggle_fullscreen_callback( void * );
    static void task_change_callback(void* program_wrapper);

private:
    bool wireframe_;
    GLuint program0_, program1_, program2_, program3_;
    GLuint vx_buf_;
    GLuint vao_;
    quat   rotation_by_control_;
    size_t current_task_num_;

    GLuint check_program();
};

