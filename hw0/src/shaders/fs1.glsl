#version 130
in vec2 vs_out_pos;
out vec3 fs_out_color;

const float size = 0.5;

void main()
{
   vec2 pos = vs_out_pos / size;
   if (((fract(pos.x) < 0.5) && (fract(pos.y) < 0.5)) || ((fract(pos.x) > 0.5) && (fract(pos.y) > 0.5))) {
      fs_out_color = vec3(1,1,1);
   } else {
      fs_out_color = vec3(0,0,0);
   }
}
