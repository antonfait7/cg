#version 130

in vec2 in_pos;
out vec2 vs_out_pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
   vs_out_pos = vec2(projection * view * vec4(in_pos, 0, 1));
   gl_Position = projection * view * model * vec4(in_pos, 0, 1);
}
