#version 130

in vec2 in_pos;
out vec3 vs_out_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

void main()
{
   gl_Position = projection * view * model * vec4(in_pos, 0, 1);
   vs_out_color = vec3(abs(sin(3 * time)), abs(sin(time + in_pos.x)), 0);
}
