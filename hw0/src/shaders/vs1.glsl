#version 130

in vec2 in_pos;
out vec2 vs_out_pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
   
   gl_Position = projection * view * model * vec4(in_pos, 0, 1);
   vs_out_pos = (gl_Position).xy;
}
