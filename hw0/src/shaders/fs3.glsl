#version 130

in vec2 pos;
out vec3 color;

uniform float scale;
uniform float x_offset;
uniform float y_offset;

void main() {
    vec2 c = vec2((pos.x + x_offset) / scale, (pos.y + y_offset) / scale);
    vec2 z = c;
    int i;
    int iter = 1000;

    for(i = 0; i < iter; i++) {
        float x = (z.x * z.x - z.y * z.y) + c.x;
        float y = (z.y * z.x + z.x * z.y) + c.y;
        if((x * x + y * y) > 4.0) break;
        z.x = x;
        z.y = y;
    }

    color = vec3(float(i) / 1000.0, float(i) / 100.0, float(i) / 1000.0);
}
