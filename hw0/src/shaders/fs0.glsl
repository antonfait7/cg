#version 130
in vec3 vs_out_color;
out vec3 fs_out_color;
void main()
{
   fs_out_color = vs_out_color;
}
