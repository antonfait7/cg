﻿
#include "program.h"

char const * program_t::VS_FILE_NAME0 = "..//src//shaders//vs0.glsl";
char const * program_t::FS_FILE_NAME0 = "..//src//shaders//fs0.glsl";

char const * program_t::VS_FILE_NAME1 = "..//src//shaders//vs1.glsl";
char const * program_t::FS_FILE_NAME1 = "..//src//shaders//fs1.glsl";

char const * program_t::VS_FILE_NAME2 = "..//src//shaders//vs2.glsl";
char const * program_t::FS_FILE_NAME2 = "..//src//shaders//fs2.glsl";

char const * program_t::VS_FILE_NAME3 = "..//src//shaders//vs3.glsl";
char const * program_t::FS_FILE_NAME3 = "..//src//shaders//fs3.glsl";

size_t const program_t::TASKS_NUM = 4;

void TW_CALL program_t::toggle_fullscreen_callback( void * ) {
    glutFullScreenToggle();
}

void program_t::task_change_callback(void* program_wrapper) {

    program_t* p = static_cast<program_t*>(program_wrapper);
    p->current_task_num_ = (p->current_task_num_ + 1) % TASKS_NUM;
}

program_t::program_t()
   : wireframe_(false),
     current_task_num_(0)
{
#ifdef USE_CORE_OPENGL
    TwInit(TW_OPENGL_CORE, NULL);
#else
    TwInit(TW_OPENGL, NULL);
#endif
    // Определение "контролов" GUI
    TwBar *bar = TwNewBar("Parameters");
    TwDefine(" Parameters size='500 150' color='70 100 120' valueswidth=220 iconpos=topleft");
    TwAddButton(bar, "Change task", task_change_callback, this,
               " label='Change task' key=w");
    TwAddVarRW(bar, "Wireframe mode", TW_TYPE_BOOLCPP, &wireframe_, " true='ON' false='OFF' key=w");
    TwAddButton(bar, "Fullscreen toggle", toggle_fullscreen_callback, NULL,
               " label='Toggle fullscreen mode' key=f");
    TwAddVarRW(bar, "ObjRotation", TW_TYPE_QUAT4F, &rotation_by_control_,
              " label='Object orientation' opened=true help='Change the object orientation.' ");



    program0_ = create_program(
                create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME0),
                create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME0));
    program1_ = create_program(
                create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME1),
                create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME1));
    program2_ = create_program(
                create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME2),
                create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME2));
    program3_ = create_program(
                create_shader(GL_VERTEX_SHADER  , VS_FILE_NAME3),
                create_shader(GL_FRAGMENT_SHADER, FS_FILE_NAME3));

    init_buffer();
    init_vertex_array();
    init_in_pos(program0_);
    init_in_pos(program1_);
    init_in_pos(program2_);
    init_in_pos(program3_);
}

program_t::~program_t()
{
    glDeleteProgram(program0_);
    glDeleteProgram(program1_);
    glDeleteProgram(program2_);
    glDeleteProgram(program3_);
    glDeleteVertexArrays(1, &vao_);
    glDeleteBuffers(1, &vx_buf_);

    TwDeleteAllBars();
    TwTerminate();
}

void program_t::init_buffer()
{
    glGenBuffers(1, &vx_buf_);
    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_);

   // Данные для визуализации
    vec2 const data[3] =
    {
        vec2(0, 1)
        , vec2(std::sqrt(3) / 2.0f, -1.0f / 2.0f)
        , vec2(-std::sqrt(3) / 2.0f, -1.0f / 2.0f)
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * 3, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void program_t::init_vertex_array()
{
    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);
    // присоединяем буфер vx_buf_ в vao
    glBindBuffer(GL_ARRAY_BUFFER, vx_buf_);

    glBindVertexArray(0);
}

void program_t::init_in_pos(GLuint program) {
    glBindVertexArray(vao_);

    // запрашиваем индек аттрибута у программы, созданные по входным шейдерам
    GLuint const pos_location = glGetAttribLocation(program, "in_pos");
    // устанавливаем формам данных для аттрибута "pos_location"
    // 2 float'а ненормализованных, шаг между вершиными равен sizeof(vec2), смещение от начала буфера равно 0
    glVertexAttribPointer(pos_location, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), 0);
    // "включаем" аттрибут "pos_location"
    glEnableVertexAttribArray(pos_location);
    glBindVertexArray(0);
}

void program_t::draw_frame(float time_from_start)
{
    float const rotation_angle = time_from_start * 70;

    float const w = (float)glutGet(GLUT_WINDOW_WIDTH);
    float const h = (float)glutGet(GLUT_WINDOW_HEIGHT);


    mat4  const proj = perspective(45.0f, w / h, 0.1f, 100.0f);
    mat4  const view = lookAt(vec3(0, 0, 8), vec3(0, 0, 0), vec3(0, 1, 0));
    // анимация по времени
    quat  const rotation_by_time = quat(vec3(0, 0, radians(rotation_angle)));
    mat4 const model = mat4_cast(rotation_by_control_ * rotation_by_time);

    if (wireframe_)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // выключаем отсечение невидимых поверхностей
    glDisable(GL_CULL_FACE);
    // выключаем тест глубины
    glDisable(GL_DEPTH_TEST);
    glClearColor(0.2f, 0.2f, 0.2f, 1);
    glClearDepth(1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    GLuint program;
    switch (current_task_num_) {
    case 0:
        program = program0_;
        break;
    case 1:
        program = program1_;
        break;
    case 2:
        program = program2_;
        break;
    case 3:
        program = program3_;
        break;
    default:
        program = program0_;
        break;
    }

    // установка шейдеров для рисования
    glUseProgram(program);

    GLuint const model_location = glGetUniformLocation(program, "model");
    glUniformMatrix4fv(model_location, 1, GL_FALSE, &model[0][0]);

    GLuint const view_location = glGetUniformLocation(program, "view");
    glUniformMatrix4fv(view_location, 1, GL_FALSE, &view[0][0]);

    GLuint const projection_location = glGetUniformLocation(program, "projection");
    glUniformMatrix4fv(projection_location, 1, GL_FALSE, &proj[0][0]);

    GLuint const time_location = glGetUniformLocation(program, "time");
    glUniform1f(time_location, time_from_start);

    if (current_task_num_ == 3) {
        float scale = 2;
        GLuint const scale_location = glGetUniformLocation(program, "scale");
        glUniform1f(scale_location, scale);

        float x_offset = 1;
        GLuint const x_offset_location = glGetUniformLocation(program, "x_offset");
        glUniform1f(x_offset_location, x_offset);

        float y_offset = -1;
        GLuint const y_offset_location = glGetUniformLocation(program, "y_offset");
        glUniform1f(y_offset_location, y_offset);

    }

    glBindVertexArray(vao_);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);
}

